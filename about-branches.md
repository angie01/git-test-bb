The following is just a specification of what's the intended for each branch. Again this all is just part of the demonstration.

## main
Contains content ready for production or that's already there. It's been proven to work and is trusted. Of course...if there was something to be deployed here...

## develop
Main branch that contains content considered as candidate to be deployed to production. All the separate work from other branches is to be merged here once finished.

## commit- prefixed branches
These are meant to show changes or uses of the commit/add and any related commmand or operation to commits and changes, like cherry pick.

## merge- prefixed branches
For merge exercises. Use this to try different types of merge strategies. Avoid trying experimental combinations with develop.

## Any other
Sample specific branches used for complex or very specific situations. Use your imagination.