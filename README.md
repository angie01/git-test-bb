# git-test (experiment)

This project exists as a way to show what git commands can do. Integrity of its content cannot be assured.

## What's a branch?
Think of them as a way to tell a tale. You got the main branch, which is for the main story. So, if you want to develop
just a single character story, then you create a branch. Once the character is ready to go back to the main story, its branch is "merged" back
into the main one.

## git commmands

Here there gonna be a list of commands and mentions of what they did affect for what branch.

### git fetch
Use this to let your local repo of what's new in the remote one.

> git fetch

### git pull
Now this, does actually download the changes for whichever branch you specify from the remote repository.

> git pull

> git pull origin main